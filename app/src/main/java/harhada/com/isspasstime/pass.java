package harhada.com.isspasstime;

/**
 * Created by Amit on 1/19/2018.
 */

public class pass {
int durationTime;
int riseTime;

    public pass(int durationTime, int riseTime) {
        this.durationTime = durationTime;
        this.riseTime = riseTime;
    }

    public pass() {

    }

    public int getDurationTime(){
        return this.durationTime;
    }
    public int getRiseTime(){
        return this.riseTime;
    }
    public void setDurationTime(int durationTime) {
        this.durationTime = durationTime;
    }

    public void setRiseTime(int riseTime) {
        this.riseTime = riseTime;
    }
}
